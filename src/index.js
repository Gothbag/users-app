import 'css/main'

import React from 'react'
import ReactDOM from 'react-dom'
import routes from 'configs/routes'
import { Provider } from 'react-redux'
import configureStore from 'configs/configureStore'
import { Router, browserHistory } from 'react-router'

const store = configureStore()

const main = () => {
  ReactDOM.render((
    <Provider store={store}>
      <Router history={browserHistory} routes={routes} />
    </Provider>
  ), document.getElementById('root'))
};

main();
