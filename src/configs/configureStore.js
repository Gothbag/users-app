/* global __DEV__ */

import { createStore, applyMiddleware, combineReducers } from "redux";
import {lists} from "reducers";
import thunk from "redux-thunk";

export default configureStore

function configureStore (initialState) {
    let createStoreWithMiddleware
    if (__DEV__) {
        createStoreWithMiddleware = applyMiddleware(thunk, require('redux-logger')())(createStore)
    } else {
        createStoreWithMiddleware = applyMiddleware(thunk)(createStore)
    }

    const reducer = combineReducers({lists})
    const store = createStoreWithMiddleware(reducer, initialState)

    return store
}
