import App from "../components/App";
import Buttons from "../components/Buttons";
import UserProfile from "../containers/UserProfile";

export default {
    childRoutes: [{
        path: '/',
        component: App,
        indexRoute: { component: Buttons },
        childRoutes: [{
            path: "/userId/:userId/gender/:gender/emisphere/:emisphere",
            component: UserProfile
        }]
    }]
};
