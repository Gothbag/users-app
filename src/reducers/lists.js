import { listActions } from "../actions/lists";

export const listConsts = {
	USERS: "users"
};

const baseInitialState = {
    elements: [],
    focus: {}
};

const initialState = {
	[listConsts.USERS]: {...baseInitialState}
};

export const elementLists = (state = initialState, action) => {
  	// make sure a list with the given id exists
	if (!state[action.listId] || !action.type) {
		return state;
	}
	const elementList = state[action.listId];
	switch (action.type) {
		case listActions.SET_LIST_FOCUS:
			return {...state, [action.listId]: {...elementList, focus: {...action.payload}}};
		default:
			return state;
	}
};

export default elementLists;
