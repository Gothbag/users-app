import {Link} from 'react-router';
import React from "react";
import { Button } from "wideeyes-ui";

const Buttons = () => {
    return (<section className='col-xs-1-1 col-md-1-3 center'>
      	<ul className="button-list">
	        <li>
	          <Link to='/userId/462/gender/Female/emisphere/South'><Button mode="primary">User 462 Female South</Button></Link>
	        </li>
	        <li>
	            <Link to='/userId/462/gender/Female/emisphere/North'><Button mode="primary">User 462 Female North</Button></Link>
	        </li>
	    </ul>
    </section>);
};

export default Buttons;
