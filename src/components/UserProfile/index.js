import React from "react";
import PropTypes from "prop-types";
import { isEmpty, noop } from "lodash";
import {Link} from 'react-router';
import { Button } from "wideeyes-ui";

class UserProfile extends React.PureComponent {

    static propTypes = {
        loadUser: PropTypes.func,
        user: PropTypes.shape({
            emisphere: PropTypes.string,
            gender: PropTypes.string,
            userId: PropTypes.string.isRequired
        }),
    }

    static defaultProps = {
        loadUser: noop,
        user: {}
    }

    componentDidMount() {
        const { props: { user, loadUser } } = this;
        if (isEmpty(user)) {
            loadUser();
        }
    }

    render() {
        const { props: { user } } = this;

        return (<section className='col-xs-1-1 col-md-1-3 center'>
            <div className="panel panel-default">
                <div className="panel-heading">User {user.userId}</div>
                <div className="panel-body">
                    <p>Gender: {user.gender}</p>
                    <p>Emisphere: {user.emisphere}</p>
                </div>
            </div>
            <Link to='/'><Button mode="primary">Back</Button></Link>
        </section>);
    }
}

export default UserProfile;
