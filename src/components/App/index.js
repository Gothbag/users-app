import {Link} from 'react-router';
import React from "react";
import { Button } from "wideeyes-ui";
import PropTypes from "prop-types";

class App extends React.PureComponent {
	render() {
		return (<section className='container' style={{height: '100%'}}>
      		<section className='row center' style={{height: '100%'}}>
	            {this.props.children}
	        </section>
	    </section>);
	}
}

App.propTypes = {
    children: PropTypes.element
};

export default App;
