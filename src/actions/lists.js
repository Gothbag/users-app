export const listActions = {
  SET_LIST_FOCUS: "lists/focus/set"
};

export const setFocus = (element, listId) => ({
  type: listActions.SET_LIST_FOCUS,
  payload: element,
  listId
});
