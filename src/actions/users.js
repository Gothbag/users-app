import { setFocus } from "./lists";
import { listConsts } from "../reducers/lists";
import { snakeToCamel } from "../helpers/camelSnake";
import {checkStatus, parseJSON} from "../utils/api";

export const loadUser = () => dispatch => new Promise((resolve, reject) => {
    return window.fetch("https://9a13faed-5ce2-470a-b95d-a9503ce21223.mock.pstmn.io/user/462/gender/Female/emisphere/North")
        .then(checkStatus)
        .then(parseJSON)
        .then(res => {
            dispatch(setFocus(snakeToCamel(res), listConsts.USERS));
        })
        .catch(err => {
            console.log(err);
            reject(err);
        })
});
