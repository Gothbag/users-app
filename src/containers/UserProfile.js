import { connect } from "react-redux"

import UserProfile from "../components/UserProfile";
import { loadUser } from "../actions/users";
import { listConsts } from "../reducers/lists";

const { USERS } = listConsts;

const mapStateToProps = state => {
	const { lists: { [USERS]: { focus } } } = state;
	return {
		user: focus
	}; 
}

const mapDispatchToProps = dispatch => ({
    loadUser: () => dispatch(loadUser())
}); 

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
